# UAQS Featured Content* (Carousel) Drupal Feature Module

(*) Name subject to change.

The UA QuickStart component for a prominent (hero) display of rotating content items.

## Features

- Provides 'featured_content' content type.
- Provides carousel view for featured_content items.
- Provides 'uaqs_hero_carousel' Flexslider preset.

## Packaged Dependencies

When this module is used as part of a Drupal distribution (such as [UA Quickstart](https://bitbucket.org/ua_drupal/ua_quickstart)), the following dependencies will be automatically packaged with the distribution.

### Drupal Contrib Modules

- [Flexslider](https://www.drupal.org/project/flexslider)

### Libraries

- [Flexslider](http://www.woothemes.com/flexslider/)
